<?php

namespace App\Models;

use Faker\Factory as Faker;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {
    private const VALID_PASSWORD = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password

    public function testActionPointsShouldBeCreatedSuccessfully() {
        $faker = new Faker;
        $faker = $faker->create();

        $name = $faker->name;
        $email = $faker->unique()->safeEmail;

        $user = new User([
            'name' => $name,
            'email' => $email,
            'password' => self::VALID_PASSWORD
        ]);

        self::assertEquals($name, $user->name);
        self::assertEquals($email, $user->email);
        self::assertEquals(self::VALID_PASSWORD, $user->password);
    }
}
