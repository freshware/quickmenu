<?php

namespace QuickMenu\Application\Services\User;

use App\Models\User;
use Faker\Factory as Faker;
use QuickMenu\Domain\Repository\User\UserRepositoryInterface;
use PHPUnit\Framework\TestCase;

class CreateUserServiceTest extends TestCase {
    private const VALID_PASS = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';

    public function testsExecuteShouldReturnAUser() {
        $faker = new Faker;
        $faker = $faker->create();

        $repo = $this->createMock(UserRepositoryInterface::class);

        $user = $repo->create([
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);

        $this->assertNotNull($user);
        $this->assertInstanceOf(User::class, $user);
    }
}
