<?php

declare(strict_types=1);

namespace QuickMenu\Domain\Exceptions\User;

use QuickMenu\Domain\Exceptions\Common\DomainException;

final class UnableToBuildUserException extends DomainException {
}
