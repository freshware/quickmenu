<?php

declare(strict_types=1);

namespace QuickMenu\Domain\Exceptions\User;

use QuickMenu\Domain\Exceptions\Common\DomainException;

final class ExistendUserException extends DomainException {
    /**
     * ExistendUserException constructor.
     *
     * @param string $email
     */
    public function __construct(
        string $email
    ) {
        parent::__construct(
            sprintf(
                "A User with email %u exists",
                $email
            )
        );
    }
}
