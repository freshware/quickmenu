<?php

namespace QuickMenu\Domain\Exceptions\Common;

use Exception;

/**
 * Domain layer exception.
 *
 * @author  Francisco Pérez <info@freshware.es>
 */
class DomainException extends Exception {
}
