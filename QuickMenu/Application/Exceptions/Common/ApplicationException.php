<?php

namespace QuickMenu\Application\Exceptions\Common;

use Exception;

/**
 * Application layer exception.
 *
 * @author  Francisco Pérez <info@freshwar.es>
 */
class ApplicationException extends Exception {
}
