<?php

namespace QuickMenu\Application\Exceptions\Common;

/**
 * Invalid resource application exception.
 *
 * @author  Francisco Pérez <info@freshwar.es>
 */
class InvalidResourceException extends ApplicationException {
}
