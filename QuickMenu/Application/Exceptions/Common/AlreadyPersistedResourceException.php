<?php

namespace QuickMenu\Application\Exceptions\Common;

use Exception;

/**
 * Already Persisted Resource exception.
 *
 * @author  Francisco Pérez <info@freshwar.es>
 */
class AlreadyPersistedResourceException extends ApplicationException {
}
