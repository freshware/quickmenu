<?php

namespace QuickMenu\Application\Services\User;

use App\Models\User;
use QuickMenu\Domain\Repository\UserRepositoryInterface;

final class CreateUserService {
    /** @var UserRepositoryInterface */
    private $userRepository;

    /**
     * CreateUserService constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function execute(array $data) : User {
        return $this->userRepository->create($data);
    }
}
