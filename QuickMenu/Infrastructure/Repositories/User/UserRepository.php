<?php

namespace QuickMenu\Infrastructure\Repositories\User;

use App\Models\User;
use Illuminate\Http\Request;
use QuickMenu\Application\Exceptions\Common\AlreadyPersistedResourceException;
use QuickMenu\Application\Exceptions\Common\InvalidResourceException;
use QuickMenu\Domain\Exceptions\User\ExistendUserException;
use QuickMenu\Domain\Exceptions\User\UnableToBuildUserException;
use QuickMenu\Domain\Repository\User\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface {
    /**
     * @param array $data
     *
     * @return User
     *
     * @throws AlreadyPersistedResourceException
     * @throws InvalidResourceException
     */
    public function create(array $data) : User {
        try {
            $user = new User([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);

            $user->save();

            return $user;
        } catch (UnableToBuildUserException $e) {
            throw new InvalidResourceException($e->getMessage(), $e->getCode(), $e);
        } catch (ExistendUserException $e) {
            throw new AlreadyPersistedResourceException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
