<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('\QuickMenu\UI\API\Controllers')->group(function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::middleware('auth:api')->group(function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::get('probando', function () {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://127.0.0.1:8000/oauth/token', [
        'form_params' => [
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'MrwFgwUsuzzinaHgqyBiVkUYNoJMPS6AOHso0ZVH',
            'username' => 'info@freshware.es',
            'password' => '12345',
            'scope' => '',
        ],
    ]);

    dd(json_decode((string) $response->getBody(), true));
});

Route::get('redireccion', function (Request $request) {
    dd($request->all());
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
